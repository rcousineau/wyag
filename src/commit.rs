use pest::Parser;
use pest_derive::Parser;

use crate::person::Person;

#[derive(Debug)]
pub struct Commit {
    pub tree: String,
    pub parents: Vec<String>,
    pub author: Person,
    pub committer: Person,
    pub message: String,
}

#[derive(Default)]
struct CommitBuilder {
    _tree: Option<String>,
    _parents: Vec<String>,
    _author: Option<Person>,
    _committer: Option<Person>,
    _message: Option<String>,
}

impl CommitBuilder {
    fn tree(&mut self, hash: &str) -> &mut Self {
        self._tree = Some(hash.to_string());
        self
    }
    fn parent(&mut self, hash: &str) -> &mut Self {
        self._parents.push(hash.to_string());
        self
    }
    fn author(&mut self, name: &str, email: &str) -> &mut Self {
        self._author = Some(Person::new(name, email));
        self
    }
    fn committer(&mut self, name: &str, email: &str) -> &mut Self {
        self._committer = Some(Person::new(name, email));
        self
    }
    fn message(&mut self, message: &str) -> &mut Self {
        self._message = Some(message.to_string());
        self
    }
    fn build(&self) -> Commit {
        Commit {
            tree: self
                ._tree
                .clone()
                .expect("CommitBuilder._tree cannot be None"),
            parents: self._parents.clone(),
            author: self
                ._author
                .clone()
                .expect("CommitBuilder._author cannot be None"),
            committer: self
                ._committer
                .clone()
                .expect("CommitBuilder._committer cannot be None"),
            message: self
                ._message
                .clone()
                .expect("CommitBuilder._message cannot be None"),
        }
    }
}

#[derive(Parser)]
#[grammar = "commit.pest"]
struct CommitParser;

pub fn parse_commit(input: &str) -> Commit {
    let mut builder = CommitBuilder::default();
    let pairs = CommitParser::parse(Rule::commit, input).expect("failed to parse");
    for pair in pairs {
        match pair.as_rule() {
            Rule::tree => {
                let mut inner = pair.into_inner();
                let hash = inner.next().unwrap();
                builder.tree(hash.as_str());
            }
            Rule::parent => {
                let mut inner = pair.into_inner();
                let hash = inner.next().unwrap();
                builder.parent(hash.as_str());
            }
            Rule::author => {
                let mut inner = pair.into_inner();
                let name = inner.next().unwrap();
                let email = inner.next().unwrap();
                builder.author(name.as_str(), email.as_str());
            }
            Rule::committer => {
                let mut inner = pair.into_inner();
                let name = inner.next().unwrap();
                let email = inner.next().unwrap();
                builder.committer(name.as_str(), email.as_str());
            }
            Rule::text => {
                builder.message(pair.as_str().trim());
            }
            _ => {}
        }
    }
    builder.build()
}
