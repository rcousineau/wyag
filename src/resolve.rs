use std::{error::Error, fs, path::Path};

#[derive(Debug)]
pub struct Reference {
    pub target: String,
    pub name: String,
}

pub fn list_refs(repo: &Path) -> Result<Vec<Reference>, Box<dyn Error>> {
    let ref_dir = repo.join("refs");
    let mut refs = list_dir_refs(&ref_dir, "refs")?;
    refs.sort_by_key(|r| r.name.clone());
    Ok(refs)
}

fn list_dir_refs(dir: &Path, prefix: &str) -> Result<Vec<Reference>, Box<dyn Error>> {
    let mut refs = Vec::new();
    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let name = format!(
            "{prefix}/{}",
            entry
                .file_name()
                .to_str()
                .expect("directory name contains invalid UTF-8")
        );
        if entry.file_type()?.is_dir() {
            let mut subdir_refs = list_dir_refs(&entry.path(), &name)?;
            refs.append(&mut subdir_refs);
        } else {
            let contents = fs::read_to_string(entry.path())?;
            if contents.starts_with("ref:") {
                let target = contents.split(' ').last().unwrap();
                // TODO: resolver for aliased refs
                println!("({:?}) found ref: {target}", entry.path());
            } else {
                refs.push(Reference {
                    target: contents.trim().to_string(),
                    name,
                })
            }
        }
    }
    Ok(refs)
}

enum Resolve {
    Hash(String),
    Reference(String),
}

pub fn resolve_ref(repo: &Path, name: &str) -> Result<Vec<String>, Box<dyn Error>> {
    if name == "HEAD" {
        let head_path = repo.join("HEAD");
        return match resolve_ref_file(&head_path)? {
            Resolve::Hash(hash) => Ok(vec![hash]),
            Resolve::Reference(ref_name) => resolve_ref(repo, &ref_name),
        };
    }
    let mut matches = Vec::new();
    for r in list_refs(repo)? {
        if r.name == name
            || r.name == format!("refs/tags/{name}")
            || r.name == format!("refs/heads/{name}")
            || r.name == format!("refs/remotes/{name}")
        {
            matches.push(r.target);
        }
    }
    if !matches.is_empty() {
        return Ok(matches);
    }
    for entry in fs::read_dir(repo.join("objects"))? {
        let entry = entry?;
        let dir_name = entry.file_name();
        let dir_name = dir_name.to_str().unwrap();
        if dir_name.len() == 2 {
            for object_entry in fs::read_dir(repo.join("objects").join(entry.file_name()))? {
                let object_entry = object_entry?;
                let file_name = object_entry.file_name();
                let file_name = file_name.to_str().unwrap();
                let hash = format!("{dir_name}{file_name}");
                if hash == name || hash.starts_with(name) {
                    matches.push(hash);
                }
            }
        }
    }
    Ok(matches)
}

fn resolve_ref_file(f: &Path) -> Result<Resolve, Box<dyn Error>> {
    let contents = fs::read_to_string(f)?;
    if contents.starts_with("ref:") {
        let target_ref = contents.trim().split(' ').last().unwrap();
        Ok(Resolve::Reference(target_ref.to_string()))
    } else {
        Ok(Resolve::Hash(contents.trim().to_string()))
    }
}
