use core::fmt;
use std::{
    error::Error,
    fs,
    io::{BufRead, BufReader, Read, Write},
    path::Path,
};

use flate2::{read::ZlibDecoder, write::ZlibEncoder, Compression};
use sha1::{Digest, Sha1};

use crate::{
    resolve::resolve_ref,
    tree::{parse_tree, TreeEntry},
};

pub enum ObjectType {
    Blob,
    Commit,
    Tag,
    Tree,
}

impl ObjectType {
    fn new(s: &str) -> Self {
        match s {
            "blob" => ObjectType::Blob,
            "commit" => ObjectType::Commit,
            "tag" => ObjectType::Tag,
            "tree" => ObjectType::Tree,
            _ => {
                panic!("invalid object type '{s}'")
            }
        }
    }
}

impl fmt::Display for ObjectType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            ObjectType::Blob => "blob",
            ObjectType::Commit => "commit",
            ObjectType::Tag => "tag",
            ObjectType::Tree => "tree",
        };
        write!(f, "{s}")
    }
}

pub enum Object {
    Item {
        object_type: ObjectType,
        data: Vec<u8>,
    },
    Tree {
        entries: Vec<TreeEntry>,
    },
}

pub fn read_object(repo: &Path, name: &str) -> Result<Object, Box<dyn Error>> {
    let hashes = resolve_ref(repo, name)?;
    if hashes.len() != 1 {
        return Err(format!("Not a valid object name: '{name}'").into());
    }
    let object_dir = &hashes[0][..2];
    let object_name = &hashes[0][2..];
    let object_path = repo.join("objects").join(object_dir).join(object_name);
    if fs::metadata(&object_path).is_err() {
        return Err(format!("Not a valid object name: '{name}'").into());
    }

    let data = fs::read(object_path)?;
    let decoder = ZlibDecoder::new(data.as_slice());
    let mut bufreader = BufReader::new(decoder);

    let mut buf = Vec::new();
    bufreader.read_until(b' ', &mut buf)?;
    buf.pop();
    let object_type = String::from_utf8(buf)?;
    let object_type = ObjectType::new(&object_type);

    let mut buf = Vec::new();
    bufreader.read_until(b'\0', &mut buf)?;
    buf.pop();
    let size = String::from_utf8(buf)?;
    let size: usize = size.parse()?;

    let mut buf = Vec::new();
    let bytes = bufreader.read_to_end(&mut buf)?;
    assert_eq!(size, bytes, "size in header should match content size");

    match object_type {
        ObjectType::Blob => Ok(Object::Item {
            object_type,
            data: buf,
        }),
        ObjectType::Commit => Ok(Object::Item {
            object_type,
            data: buf,
        }),
        ObjectType::Tag => Ok(Object::Item {
            object_type,
            data: buf,
        }),
        ObjectType::Tree => {
            let entries = parse_tree(&buf)?;
            Ok(Object::Tree { entries })
        }
    }
}

pub fn write_object(
    repo: Option<&Path>,
    object_type: &str,
    contents: &[u8],
) -> Result<String, Box<dyn Error>> {
    let mut header = Vec::new();
    let mut object_type = object_type.as_bytes().to_vec();
    header.append(&mut object_type);
    header.push(b' ');
    let mut size = format!("{}", contents.len()).as_bytes().to_vec();
    header.append(&mut size);
    header.push(b'\0');
    let mut contents = contents.to_vec();
    header.append(&mut contents);

    let hash = Sha1::digest(&header);
    let hash = hex::encode(hash);

    if let Some(repo) = repo {
        let object_dir = &hash[..2];
        let object_name = &hash[2..];
        let object_path = repo.join("objects").join(object_dir).join(object_name);
        fs::create_dir_all(repo.join("objects").join(object_dir))?;
        let mut encoder = ZlibEncoder::new(Vec::new(), Compression::default());
        encoder.write_all(&header)?;
        let data = encoder.finish()?;
        fs::write(object_path, data)?;
    }

    Ok(hash)
}
