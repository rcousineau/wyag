#[derive(Clone, Debug)]
pub struct Person {
    pub name: String,
    pub email: String,
}

impl Person {
    pub fn new(name: &str, email: &str) -> Self {
        Person {
            name: name.to_string(),
            email: email.to_string(),
        }
    }
}
