use std::{
    error::Error,
    fs,
    path::{Path, PathBuf},
};

pub fn create_repo(name: Option<String>) -> Result<PathBuf, Box<dyn Error>> {
    let mut git_dir = PathBuf::from(".git");

    if let Some(ref name) = name {
        fs::create_dir_all(name)?;
        git_dir = Path::new(name).join(git_dir);
    }

    // TODO: "or reinitialize an existing one"
    // https://git-scm.com/docs/git-init
    fs::create_dir(&git_dir)?;
    fs::create_dir(git_dir.join("objects"))?;
    fs::create_dir(git_dir.join("refs"))?;
    fs::write(git_dir.join("HEAD"), "ref: refs/heads/main\n")?;

    Ok(git_dir)
}

pub fn find_repo(dir: &str) -> Result<PathBuf, Box<dyn Error>> {
    let git_dir = Path::new(dir).join(".git");
    if let Ok(metadata) = fs::metadata(&git_dir) {
        if !metadata.is_dir() {
            let git_path = fs::canonicalize(&git_dir)?;
            let git_path = git_path.to_str().unwrap();
            return Err(format!("found regular file {}", git_path).into());
        }
        return Ok(git_dir);
    }
    find_repo("..")
}
