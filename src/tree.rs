use core::fmt;
use std::{
    error::Error,
    io::{BufRead, BufReader, Read},
};

pub struct TreeEntry {
    pub mode: String,
    pub entry_type: String,
    pub path: String,
    pub hash: String,
}

impl fmt::Display for TreeEntry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:0>6} {} {}\t{}",
            self.mode, self.entry_type, self.hash, self.path
        )
    }
}

pub fn parse_tree(input: &[u8]) -> Result<Vec<TreeEntry>, Box<dyn Error>> {
    let mut tree = Vec::new();

    let mut bufreader = BufReader::new(input);

    while let Some(entry) = parse_entry(&mut bufreader)? {
        tree.push(entry);
    }

    Ok(tree)
}

fn parse_entry(bufreader: &mut BufReader<&[u8]>) -> Result<Option<TreeEntry>, Box<dyn Error>> {
    let mut buf = Vec::new();
    bufreader.read_until(b' ', &mut buf)?;
    if buf.is_empty() {
        return Ok(None);
    }
    buf.pop();
    let mode = String::from_utf8(buf)?;

    // TODO: there has to be a better way to check type
    let entry_type = if mode.starts_with("10") {
        String::from("blob")
    } else {
        String::from("tree")
    };

    let mut buf = Vec::new();
    bufreader.read_until(b'\0', &mut buf)?;
    buf.pop();
    let path = String::from_utf8(buf)?;

    let mut buf: [u8; 20] = [0; 20];
    bufreader.read_exact(&mut buf)?;
    let hash = hex::encode(buf);

    Ok(Some(TreeEntry {
        mode,
        entry_type,
        path,
        hash,
    }))
}
