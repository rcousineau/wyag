use std::{
    error::Error,
    fs,
    io::{self, Write},
};

use clap::{Parser, Subcommand};
use wyag::{
    object::{read_object, write_object, Object, ObjectType},
    person::Person,
    repo::{create_repo, find_repo},
    resolve::{list_refs, resolve_ref},
    tag::{serialize_tag, Tag},
};

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[command(subcommand)]
    command: Command,
}

#[derive(Debug, Subcommand)]
enum Command {
    #[command(about = "Create an empty Git repository")]
    Init { name: Option<String> },
    #[command(about = "Provide contents or details of repository objects")]
    CatFile {
        #[arg(
            short,
            help = "Pretty-print the contents of <object> based on its type"
        )]
        pretty_print: bool,
        #[arg(
            short = 't',
            help = "Instead of the content, show the object type identified by <object>"
        )]
        show_type: bool,
        #[arg(value_name = "object", help = "The name of the object to show")]
        object: String,
    },
    #[command(about = "Compute object ID and optionally create an object from a file")]
    HashObject {
        #[arg(
            short = 't',
            value_name = "type",
            value_parser = ["commit", "tree", "blob", "tag"],
            default_value = "blob",
            help = "Specify the type of object to be created"
        )]
        object_type: String,
        #[arg(short, help = "Actually write the object into the object database")]
        write: bool,
        file: String,
    },
    #[command(about = "List the contents of a tree object")]
    LsTree { tree: String },
    #[command(about = "Pick out and massage parameters")]
    RevParse { rev: String },
    #[command(about = "List references in a local repository")]
    ShowRef,
    #[command(about = "Create, list, or delete a tag object")]
    Tag {
        #[arg(
            short,
            help = "Use the given tag message. Makes an annotated tag object"
        )]
        message: Option<String>,
        #[arg(value_name = "tagname")]
        tag_name: Option<String>,
        #[arg(value_name = "object")]
        object: Option<String>,
    },
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    match args.command {
        Command::Init { name } => {
            let path = create_repo(name)?;
            println!(
                "Initialized empty Git repository in {}",
                path.to_str().expect("repository path is not valid UTF-8")
            );
        }
        Command::CatFile {
            pretty_print,
            show_type,
            object,
        } => {
            if !pretty_print && !show_type {
                unimplemented!("don't know how to cat-file without -p yet")
            }
            let repo = find_repo(".")?;
            let object = read_object(&repo, &object)?;
            match object {
                Object::Tree { entries } => {
                    if show_type {
                        println!("tree");
                    } else {
                        for entry in entries {
                            println!("{entry}");
                        }
                    }
                }
                Object::Item { object_type, data } => {
                    if show_type {
                        println!("{object_type}");
                    } else {
                        let contents = String::from_utf8(data)?;
                        print!("{contents}");
                        io::stdout().flush()?;
                    }
                }
            }
        }
        Command::HashObject {
            object_type,
            write,
            file,
        } => {
            let contents = fs::read(file)?;
            let repo = if write {
                let repo = find_repo(".")?;
                Some(repo)
            } else {
                None
            };
            let hash = write_object(repo.as_deref(), &object_type, &contents)?;
            println!("{hash}");
        }
        Command::LsTree { tree } => {
            // TODO: handle -r flag
            let repo = find_repo(".")?;
            let object = read_object(&repo, &tree)?;
            match object {
                Object::Tree { entries } => {
                    for entry in entries {
                        println!("{entry}");
                    }
                }
                _ => return Err("not a tree object".into()),
            }
        }
        Command::RevParse { rev } => {
            let repo = find_repo(".")?;
            let hashes = resolve_ref(&repo, &rev)?;
            if hashes.len() != 1 {
                return Err(format!("Not a valid object name: '{rev}'").into());
            }
            println!("{}", hashes[0]);
        }
        Command::ShowRef => {
            let repo = find_repo(".")?;
            let refs = list_refs(&repo)?;
            for r in refs {
                println!("{} {}", r.target, r.name);
            }
        }
        Command::Tag {
            message,
            tag_name,
            object,
        } => {
            let repo = find_repo(".")?;
            match tag_name {
                Some(tag_name) => {
                    let target = match object {
                        Some(hash) => hash,
                        None => String::from("HEAD"),
                    };
                    let target_type = match read_object(&repo, &target) {
                        Ok(object) => match object {
                            Object::Item { object_type, .. } => Some(object_type),
                            Object::Tree { .. } => Some(ObjectType::Tree),
                        },
                        Err(_) => {
                            return Err(
                                format!("Failed to resolve '{target}' as a valid ref").into()
                            );
                        }
                    };
                    match message {
                        Some(message) => {
                            let tag = Tag {
                                object: target,
                                object_type: target_type.unwrap().to_string(),
                                tag: tag_name.clone(),
                                // TODO: read git config
                                tagger: Person::new("wyag_user", "wyag_user@wyag.com"),
                                message,
                            };
                            let tag_data = serialize_tag(tag);
                            let hash = write_object(Some(&repo), "tag", &tag_data)?;
                            let tags_dir = repo.join("refs").join("tags");
                            fs::create_dir_all(&tags_dir)?;
                            let ref_path = tags_dir.join(tag_name);
                            fs::write(ref_path, hash)?;
                        }
                        None => {
                            let tags_dir = repo.join("refs").join("tags");
                            fs::create_dir_all(&tags_dir)?;
                            let ref_path = tags_dir.join(tag_name);
                            fs::write(ref_path, target)?;
                        }
                    }
                }
                None => {
                    // just list tags
                    let refs = list_refs(&repo)?;
                    for tag in refs
                        .iter()
                        .filter(|r| r.name.starts_with("refs/tags"))
                        .map(|r| r.name.replace("refs/tags/", ""))
                    {
                        println!("{tag}");
                    }
                }
            }
        }
    }

    Ok(())
}
