use std::time::SystemTime;

use pest::Parser;
use pest_derive::Parser;

use crate::person::Person;

#[derive(Debug)]
pub struct Tag {
    pub object: String,
    pub object_type: String,
    pub tag: String,
    pub tagger: Person,
    pub message: String,
}

#[derive(Default)]
struct TagBuilder {
    _object: Option<String>,
    _object_type: Option<String>,
    _tag: Option<String>,
    _tagger: Option<Person>,
    _message: Option<String>,
}

impl TagBuilder {
    fn object(&mut self, hash: &str) -> &mut Self {
        self._object = Some(hash.to_string());
        self
    }
    fn object_type(&mut self, object_type: &str) -> &mut Self {
        self._object_type = Some(object_type.to_string());
        self
    }
    fn tag(&mut self, tag: &str) -> &mut Self {
        self._tag = Some(tag.to_string());
        self
    }
    fn tagger(&mut self, name: &str, email: &str) -> &mut Self {
        self._tagger = Some(Person::new(name, email));
        self
    }
    fn message(&mut self, message: &str) -> &mut Self {
        self._message = Some(message.to_string());
        self
    }
    fn build(&self) -> Tag {
        Tag {
            object: self
                ._object
                .clone()
                .expect("TagBuilder._object cannot be None"),
            object_type: self
                ._object_type
                .clone()
                .expect("TagBuilder._object_type cannot be None"),
            tag: self._tag.clone().expect("TagBuilder._tag cannot be None"),
            tagger: self
                ._tagger
                .clone()
                .expect("TagBuilder._tagger cannot be None"),
            message: self
                ._message
                .clone()
                .expect("TagBuilder._message cannot be None"),
        }
    }
}

#[derive(Parser)]
#[grammar = "tag.pest"]
struct TagParser;

pub fn parse_tag(input: &str) -> Tag {
    let mut builder = TagBuilder::default();
    let pairs = TagParser::parse(Rule::tag_file, input).expect("failed to parse");
    for pair in pairs {
        match pair.as_rule() {
            Rule::object => {
                let mut inner = pair.into_inner();
                let hash = inner.next().unwrap();
                builder.object(hash.as_str());
            }
            Rule::type_decl => {
                let mut inner = pair.into_inner();
                let object_type = inner.next().unwrap();
                builder.object_type(object_type.as_str());
            }
            Rule::tag_decl => {
                let mut inner = pair.into_inner();
                let tag = inner.next().unwrap();
                builder.tag(tag.as_str());
            }
            Rule::tagger => {
                let mut inner = pair.into_inner();
                let name = inner.next().unwrap();
                let email = inner.next().unwrap();
                builder.tagger(name.as_str(), email.as_str());
            }
            Rule::text => {
                builder.message(pair.as_str().trim());
            }
            _ => {}
        }
    }
    builder.build()
}

pub fn serialize_tag(tag: Tag) -> Vec<u8> {
    let mut s = String::new();
    let object = format!("object {}\n", tag.object);
    s.push_str(&object);
    let object_type = format!("type {}\n", tag.object_type);
    s.push_str(&object_type);
    let tag_name = format!("tag {}\n", tag.tag);
    s.push_str(&tag_name);
    let timestamp = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs();
    let tagger = format!(
        "tagger {} <{}> {} -0700\n",
        tag.tagger.name, tag.tagger.email, timestamp
    );
    s.push_str(&tagger);
    s.push('\n');
    let message = format!("{}\n", tag.message);
    s.push_str(&message);
    s.into_bytes()
}
