# wyag

following along with [Write yourself a Git!](https://wyag.thb.lt/)

## demo

create a repository:

```sh
➜ wyag init example
Initialized empty Git repository in example/.git

➜ cd example

# git interop!
➜ git status
On branch main

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```

add a blob object to the repository:

```sh
➜ echo 'hello' > README

➜ wyag hash-object -w README
ce013625030ba8dba906f756967f9e9ca394464a

➜ wyag cat-file -p ce013625030ba8dba906f756967f9e9ca394464a
hello

# git interop!
➜ git cat-file -p ce013625030ba8dba906f756967f9e9ca394464a
hello
```

NOTE: `wyag add` and `wyag commit` not implemented yet

add commit to the repository:

```sh
➜ git add README
➜ git commit -m 'initial commit'
[main (root-commit) 81fe304] initial commit

# wyag interop!
➜ wyag rev-parse HEAD
81fe304b396ec3885a959509314aef002df95dea

➜ wyag cat-file -p 81fe304b396ec3885a959509314aef002df95dea
tree 7d4a466af82cd6857c85c0296d5c23fc68cba887
author Russell <miller.time.baby@gmail.com> 1712590950 -0700
committer Russell <miller.time.baby@gmail.com> 1712590950 -0700

initial commit

➜ wyag ls-tree 7d4a466af82cd6857c85c0296d5c23fc68cba887
100644 blob ce013625030ba8dba906f756967f9e9ca394464a    README
```

NOTE: `wyag tag` does not read git config to set the tagger

add tag to the repository:

```sh
➜ wyag tag -m 'releasing 0.0.1' 0.0.1

➜ wyag show-ref
81fe304b396ec3885a959509314aef002df95dea refs/heads/main
9c5d805c72dde1fd46c504c189a5025ce841b993 refs/tags/0.0.1

➜ wyag cat-file -p 9c5d805c72dde1fd46c504c189a5025ce841b993
object HEAD
type commit
tag 0.0.1
tagger wyag_user <wyag_user@wyag.com> 1712591218 -0700

releasing 0.0.1

# git interop!
➜ git show-ref
81fe304b396ec3885a959509314aef002df95dea refs/heads/main
9c5d805c72dde1fd46c504c189a5025ce841b993 refs/tags/0.0.1

➜ git cat-file -p 9c5d805c72dde1fd46c504c189a5025ce841b993
object HEAD
type commit
tag 0.0.1
tagger wyag_user <wyag_user@wyag.com> 1712591218 -0700

releasing 0.0.1
```
